$(document).ready(function(){
    for(let i=8; i<20;i++) {
        $('#temporada').append('<option value="'+i+'">'+i+'ª</option>');
    }
});
var jogadores = [];
var query = firebase.database().ref("jogadores/t"+temporada).orderByChild("nickname");
query.once("value")
.then(function(snapshot) {
    snapshot.forEach(function(childSnapshot) {
        var key = childSnapshot.key;
        var jogador = childSnapshot.val();
        jogadores.push(jogador);
    });
    $(document).ready(function() {
        // console.log(jogadores);
        for(let jogador of jogadores){
            // JOGADORES DAS PERSONALIZADAS
            var linha = $("<tr></tr>");
            var colCheck = $('<td><div class=""> <input type="checkbox" class="" value="'+jogador.nickname+'"> </div></td>');
            $(linha).append(colCheck);
            var colNome = $('<th></th>').text(jogador.nome);
            $(linha).append(colNome);
            var colNick = $('<td></td>').text(jogador.nickname);
            $(linha).append(colNick);
            var colVit = $("<td></td>").text(jogador.vitorias);
            $(linha).append(colVit);
            var colSolo = $("<td ></td>").text(jogador.solo);
            $(linha).append(colSolo);
            var colDuo = $("<td ></td>").text(jogador.duo);
            $(linha).append(colDuo);
            var colSquad = $("<td ></td>").text(jogador.squad);
            $(linha).append(colSquad);
            // var colAdd = $('');
            // $(linha).append(colAdd);
            $("#tb_jogadores").append(linha);
        }
    });
});


/**
 * aqui fica a função de dar ipdate nos usuários já existes no banco de dados
 * atualizar vitorias
 * adionar ou remover uma vitoria dos jogadores
 * atualizar vitorias
 */

 // adicionar +1 para solo, duo ou squad
$('#add_vit_solo').click(function(){
    var jog_vit = [];
    $(':checked').each(function(){
        if($(this).val()!="Escolha uma temporada"){
            jog_vit.push($(this).val());
        }
    });

    for(let j of jog_vit){
        firebase.database().ref('jogadores/t'+ temporada +'/'+j).once('value').then(function(snapshot) {
            var nickname = snapshot.val().nickname || 'Anonymous';
            // var solo = snapshot.val().solo;
            // console.log(nickname);
            var solo = snapshot.val().solo;
            var vitorias = snapshot.val().vitorias;
            var jogador_atualizado = {};
            jogador_atualizado['jogadores/t' +temporada +'/'+j+'/solo/'] = solo + 1;
            jogador_atualizado['jogadores/t' + temporada +'/'+j+'/vitorias/'] = vitorias + 1;
            firebase.database().ref().update(jogador_atualizado);
        });
    }
    alert('Operação realizada com sucesso!!!');
    location.reload();
});

$('#add_vit_duo').click(function(){
    var jog_vit = [];
    $(':checked').each(function(){
        if($(this).val()!="Escolha uma temporada"){
            jog_vit.push($(this).val());
        }
    });

    for(let j of jog_vit){
        firebase.database().ref('jogadores/t' + temporada +'/'+j).once('value').then(function(snapshot) {
            var nickname = snapshot.val().nickname || 'Anonymous';
            // var duo = snapshot.val().duo;
            // console.log(nickname);
            var duo = snapshot.val().duo;
            var vitorias = snapshot.val().vitorias;
            var jogador_atualizado = {};
            jogador_atualizado['jogadores/t' + temporada +'/'+j+'/duo/'] = duo + 1;
            jogador_atualizado['jogadores/t' + temporada +'/'+j+'/vitorias/'] = vitorias + 1;
            firebase.database().ref().update(jogador_atualizado);
        });
    }
    alert('Operação realizada com sucesso!!!');
    location.reload();
});

$('#add_vit_squad').click(function(){
    var jog_vit = [];
    $(':checked').each(function(){
        if($(this).val()!="Escolha uma temporada"){
            jog_vit.push($(this).val());
        }
    });

    for(let j of jog_vit){
        firebase.database().ref('jogadores/t' + temporada +'/'+j).once('value').then(function(snapshot) {
            var nickname = snapshot.val().nickname || 'Anonymous';
            // var squad = snapshot.val().squad;
            // console.log(nickname);
            var squad = snapshot.val().squad;
            var vitorias = snapshot.val().vitorias;
            var jogador_atualizado = {};
            jogador_atualizado['jogadores/t' + temporada +'/'+j+'/squad/'] = squad + 1;
            jogador_atualizado['jogadores/t' + temporada +'/'+j+'/vitorias/'] = vitorias + 1;
            firebase.database().ref().update(jogador_atualizado);
        });
    }
    alert('Operação realizada com sucesso!!!');
    location.reload();
});

// remover -1 de solo, duo ou squad

$('#rmv_vit_solo').click(function(){
    var jog_rmv = [];
    $(':checked').each(function(){
        if($(this).val()!="Escolha uma temporada"){
            jog_rmv.push($(this).val());
        }
    });

    for(let j of jog_rmv){
        firebase.database().ref('jogadores/t' + temporada +'/'+j).once('value').then(function(snapshot) {
            var nickname = snapshot.val().nickname || 'Anonymous';
            // var solo = snapshot.val().solo;
            // console.log(nickname);
            var solo = snapshot.val().solo;
            var vitorias = snapshot.val().vitorias;
            var jogador_atualizado = {};
            jogador_atualizado['jogadores/t' + temporada +'/'+j+'/solo/'] = solo - 1;
            jogador_atualizado['jogadores/t' + temporada +'/'+j+'/vitorias/'] = vitorias - 1;
            firebase.database().ref().update(jogador_atualizado);
        });
    }
    alert('Operação realizada com sucesso!!!');
    location.reload();
});

$('#rmv_vit_duo').click(function(){
    var jog_rmv = [];
    $(':checked').each(function(){
        if($(this).val()!="Escolha uma temporada"){
            jog_rmv.push($(this).val());
        }
    });

    for(let j of jog_rmv){
        firebase.database().ref('jogadores/t' + temporada +'/'+j).once('value').then(function(snapshot) {
            var nickname = snapshot.val().nickname || 'Anonymous';
            // var duo = snapshot.val().duo;
            // console.log(nickname);
            var duo = snapshot.val().duo;
            var vitorias = snapshot.val().vitorias;
            var jogador_atualizado = {};
            jogador_atualizado['jogadores/t' + temporada +'/'+j+'/duo/'] = duo - 1;
            jogador_atualizado['jogadores/t' + temporada +'/'+j+'/vitorias/'] = vitorias - 1;
            firebase.database().ref().update(jogador_atualizado);
        });
    }
    alert('Operação realizada com sucesso!!!');
    location.reload();
});

$('#rmv_vit_squad').click(function(){
    var jog_rmv = [];
    $(':checked').each(function(){
        if($(this).val()!="Escolha uma temporada"){
            jog_rmv.push($(this).val());
        }
    });

    for(let j of jog_rmv){
        firebase.database().ref('jogadores/t' + temporada +'/'+j).once('value').then(function(snapshot) {
            var nickname = snapshot.val().nickname || 'Anonymous';
            // var squad = snapshot.val().squad;
            // console.log(nickname);
            var squad = snapshot.val().squad;
            var vitorias = snapshot.val().vitorias;
            var jogador_atualizado = {};
            jogador_atualizado['jogadores/t' + temporada +'/'+j+'/squad/'] = squad - 1;
            jogador_atualizado['jogadores/t' + temporada +'/'+j+'/vitorias/'] = vitorias - 1;
            firebase.database().ref().update(jogador_atualizado);
        });
    }
    alert('Operação realizada com sucesso!!!');
    location.reload();
});

'use strict';
function compara(modo, vetor) {

    vetor.sort(function comparar(a, b) {
        if (a.kill > b.kill) {
            return -1;
        }
        if (a.kill < b.kill) {
            return 1;
        }
        // a deve ser igual a b
        return 0;
    });

    if (modo == 'solo') {
        vetor.sort(function comparar(a, b) {
            if (a.solo > b.solo) {
                return -1;
            }
            if (a.solo < b.solo) {
                return 1;
            }
            // a deve ser igual a b
            return 0;
        });
    } else if (modo == 'duo') {
        vetor.sort(function comparar(a, b) {
            if (a.duo > b.duo) {
                return -1;
            }
            if (a.duo < b.duo) {
                return 1;
            }
            // a deve ser igual a b
            return 0;
        });
    } else if (modo == 'kill') {
        vetor.sort(function comparar(a, b) {
            if (a.kill > b.kill) {
                return -1;
            }
            if (a.kill < b.kill) {
                return 1;
            }
            // a deve ser igual a b
            return 0;
        });
    } else if (modo == 'squad') {
        vetor.sort(function comparar(a, b) {
            if (a.squad > b.squad) {
                return -1;
            }
            if (a.squad < b.squad) {
                return 1;
            }
            // a deve ser igual a b
            return 0;
        });
    } else {
        vetor.sort(function comparar(a, b) {
            if (a.vitorias > b.vitorias) {
                return -1;
            }
            if (a.vitorias < b.vitorias) {
                return 1;
            }
            // a deve ser igual a b
            return 0;
        });
    }
    return vetor;
}

jogadores.sort(function comparar(a, b) {
    if (a.nome < b.nome) {
        return -1;
    }
    if (a.nome > b.nome) {
        return 1;
    }
    // a deve ser igual a b
        return 0;
});

jogadores.sort(function comparar(a, b) {
    a.vitorias = a.solo+a.duo+a.squad;
    b.vitorias = b.solo+b.duo+b.squad;
    if (a.vitorias > b.vitorias) {
        return -1;
    }
    if (a.vitorias < b.vitorias) {
        return 1;
    }
    // a deve ser igual a b
        return 0;
});
var ctx = document.getElementById("rank").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: [jogadores[0].nome, jogadores[1].nome, jogadores[2].nome, jogadores[3].nome, jogadores[4].nome, jogadores[5].nome],
    datasets: [{
      label: 'Vitórias',
      data: [jogadores[0].vitorias, jogadores[1].vitorias, jogadores[2].vitorias, jogadores[3].vitorias, jogadores[4].vitorias, jogadores[5].vitorias],
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
    }]
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }
});

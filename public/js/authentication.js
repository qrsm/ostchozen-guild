var authEmailPassButton = $('#authEmailPassButton').val();
var authFacebookButton = $('#authFacebookButton').val();
var authTwitterButton = $('#authTwitterButton').val();
var authGoogleButton = $('#authGoogleButton').val();
var authAnonymouslyButton = $('#authAnonymouslyButton').val();
var createUserButton = $('#createUserButton').val();
var logOutButton = $('#logOutButton');

// inputs
var emailInput = document.getElementById('emailInput');
var passwordInput = $('#passwordInput');

// Displays
var displayMsg = $('#displayMsg');

// criar conta
$('#createUserButton').click(function(){
    firebase
        .auth()
        .createUserWithEmailAndPassword(emailInput.value, passwordInput.val())
        .then(function(){
            alert('Bem vindo '+ emailInput.value);
        })
        .catch(function(error){
            console.error(error.code);
            console.error(error.message);
            alert('Falha ao cadastrar, verifique o erro no console.')
        });
});

// entrar com email
$('#authEmailPassButton').click(function(){
    firebase
        .auth()
        .signInWithEmailAndPassword(emailInput.value, passwordInput.val())
        .then(function(){
            alert('Autenticado '+ emailInput.value);
        })
        .catch(function(error){
            console.error(error.code);
            console.error(error.message);
            alert('Erro de autenticação, verifique se seu email ou senha estão corretos.')
        });
});


// sair
$('#logOutButton').click(function(){
    firebase
        .auth()
        .signOut()
        .then(function(){
            alert('Você deslogou');
        },function(error){
            console.error(error.code);
        });
});


//função para autenticar com redesocias
function signIn(provider) {
    firebase
        .auth()
        .signInWithPopup(provider)
        .then(function(result){
            console.log(result);
            var token = result.credencial.acessToken;
            alert('Autenticação com redesocial bem sucessida, '+ result.user.displayName+'!');
        }).catch(function(error){
            console.log(error);
            alert('Fala na autenticação, verifique o console');
        });
}


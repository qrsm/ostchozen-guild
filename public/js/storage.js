// ouvir o evento change
$('#fileButton').change(function(e){
    // obter o arquivo carregado no input
    var file = e.target.files[0];

    // referenciar o storage
    var storageRef = firebase.storage().ref('imagens/' + file.name);

    // enviar o arquivo; necessário por sicronizar com o progressbar
    var task = storageRef.put(file);

    // atualizar o progressbar
    task.on( 'state_changed',
    function progress(snapshot) {
        var percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        $('#uploader').val(percentage);
    },
    function error(err){
        console.log(err);
    },
    function complete(){
        alert('Envio completo!')
    }
    );
});

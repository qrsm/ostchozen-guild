// card melhor jogador geral 1

var jogadores = [];
var query = firebase.database().ref("jogadores/t" + temporada).orderByChild("nickname");
query.once("value")
    .then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
            var key = childSnapshot.key;
            var jogador = childSnapshot.val();
            jogadores.push(jogador);
            var jogadores_lista = compara('', jogadores);
            var i = 1;
            for (let jogador of jogadores_lista) {
                $('#nome-melhor-geral' + i).text(jogador.nickname); // pões o nickname do jogador no card 1
                $('#solo-melhor-geral' + i).html('<i class="fas fa-user"> </i> ' + jogador.solo); // mostra quantidade de vitórias no solo
                $('#duo-melhor-geral' + i).html('<i class="fas fa-user-friends"> </i> ' + jogador.duo); // mostra quantidade de vitórias no solo
                $('#squad-melhor-geral' + i).html('<i class="fas fa-users"> </i> ' + jogador.squad); // mostra quantidade de vitórias no solo
                ++i;
                if (i == 4) { break; };
            }
        });
    });

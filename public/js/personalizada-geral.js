'use strict';


var jogadores = [];
var query = firebase.database().ref("jogadores/t" + temporada).orderByChild("vitorias");
query.once("value")
    .then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
            var key = childSnapshot.key;
            var jogador = childSnapshot.val();
            jogadores.push(jogador);
        });
        $(document).ready(function () {
            var i = 1;
            var lista_jogadores = compara('', jogadores);
            // console.log(jogadores);
            for (let jogador of jogadores) {
                // JOGADORES DAS PERSONALIZADAS
                var linha = $("<tr></tr>");
                var colNum = $("<th scope=\"row\"></th>").text(i);
                $(linha).append(colNum);
                var colNome = $("<td style=\"font-weight: bolder;\"></td>").text(jogador.nickname);
                $(linha).append(colNome);
                var colSolo = $("<td></td>").text(jogador.solo);
                $(linha).append(colSolo);
                var colDuo = $("<td></td>").text(jogador.duo);
                $(linha).append(colDuo);
                var colSquad = $("<td></td>").text(jogador.squad);
                $(linha).append(colSquad);
                var kill = $("<td></td>").text(jogador.kill);
                $(linha).append(kill);
                var colVit = $("<td style=\"background-color:#EFEFEF\"></td>").text(jogador.vitorias);
                $(linha).append(colVit);
                $("#tabela-geral").append(linha);
                i++;
            }
            $('#tab-squad').click(function () {
                $("#tabela-geral").empty();
                var i = 1;
                var lista_jogadores = compara('squad', jogadores);
                for (let jogador of jogadores) {
                    // JOGADORES DAS PERSONALIZADAS
                    var linha = $("<tr></tr>");
                    var colNum = $("<th scope=\"row\"></th>").text(i);
                    $(linha).append(colNum);
                    var colNome = $("<td style=\"font-weight: bolder;\"></td>").text(jogador.nickname);
                    $(linha).append(colNome);
                    var colSolo = $("<td></td>").text(jogador.solo);
                    $(linha).append(colSolo);
                    var colDuo = $("<td></td>").text(jogador.duo);
                    $(linha).append(colDuo);
                    var colSquad = $("<td style=\"background-color:#EFEFEF\"></td>").text(jogador.squad);
                    $(linha).append(colSquad);
                    var kill = $("<td></td>").text(jogador.kill);
                    $(linha).append(kill);
                    var colVit = $("<td></td>").text(jogador.vitorias);
                    $(linha).append(colVit);
                    $("#tabela-geral").append(linha);
                    i++;
                }
            });

            $('#tab-solo').click(function () {
                $("#tabela-geral").empty();
                var i = 1;
                var lista_jogadores = compara('solo', jogadores);
                for (let jogador of jogadores) {
                    // JOGADORES DAS PERSONALIZADAS
                    var linha = $("<tr></tr>");
                    var colNum = $("<th scope=\"row\"></th>").text(i);
                    $(linha).append(colNum);
                    var colNome = $("<td style=\"font-weight: bolder;\"></td>").text(jogador.nickname);
                    $(linha).append(colNome);
                    var colSolo = $("<td style=\"background-color:#EFEFEF\"></td>").text(jogador.solo);
                    $(linha).append(colSolo);
                    var colDuo = $("<td></td>").text(jogador.duo);
                    $(linha).append(colDuo);
                    var colSquad = $("<td ></td>").text(jogador.squad);
                    $(linha).append(colSquad);
                    var kill = $("<td></td>").text(jogador.kill);
                    $(linha).append(kill);
                    var colVit = $("<td></td>").text(jogador.vitorias);
                    $(linha).append(colVit);
                    $("#tabela-geral").append(linha);
                    i++;
                }
            });

            $('#tab-duo').click(function () {
                $("#tabela-geral").empty();
                var i = 1;
                var lista_jogadores = compara('duo', jogadores);
                for (let jogador of jogadores) {
                    // JOGADORES DAS PERSONALIZADAS
                    var linha = $("<tr></tr>");
                    var colNum = $("<th scope=\"row\"></th>").text(i);
                    $(linha).append(colNum);
                    var colNome = $("<td style=\"font-weight: bolder;\"></td>").text(jogador.nickname);
                    $(linha).append(colNome);
                    var colSolo = $("<td ></td>").text(jogador.solo);
                    $(linha).append(colSolo);
                    var colDuo = $("<td style=\"background-color:#EFEFEF\"></td>").text(jogador.duo);
                    $(linha).append(colDuo);
                    var colSquad = $("<td ></td>").text(jogador.squad);
                    $(linha).append(colSquad);
                    var kill = $("<td></td>").text(jogador.kill);
                    $(linha).append(kill);
                    var colVit = $("<td></td>").text(jogador.vitorias);
                    $(linha).append(colVit);
                    $("#tabela-geral").append(linha);
                    i++;
                }
            });

            $('#tab-vitoria').click(function () {
                $("#tabela-geral").empty();
                var i = 1;
                var lista_jogadores = compara('', jogadores);
                for (let jogador of jogadores) {
                    // JOGADORES DAS PERSONALIZADAS
                    var linha = $("<tr></tr>");
                    var colNum = $("<th scope=\"row\"></th>").text(i);
                    $(linha).append(colNum);
                    var colNome = $("<td style=\"font-weight: bolder;\"></td>").text(jogador.nickname);
                    $(linha).append(colNome);
                    var colSolo = $("<td ></td>").text(jogador.solo);
                    $(linha).append(colSolo);
                    var colDuo = $("<td ></td>").text(jogador.duo);
                    $(linha).append(colDuo);
                    var colSquad = $("<td ></td>").text(jogador.squad);
                    $(linha).append(colSquad);
                    var kill = $("<td></td>").text(jogador.kill);
                    $(linha).append(kill);
                    var colVit = $("<td style=\"background-color:#EFEFEF\"></td>").text(jogador.vitorias);
                    $(linha).append(colVit);
                    $("#tabela-geral").append(linha);
                    i++;
                }
            });

            $('#tab-kill').click(function () {
                $("#tabela-geral").empty();
                var i = 1;
                var lista_jogadores = compara('kill', jogadores);
                for (let jogador of jogadores) {
                    // JOGADORES DAS PERSONALIZADAS
                    var linha = $("<tr></tr>");
                    var colNum = $("<th scope=\"row\"></th>").text(i);
                    $(linha).append(colNum);
                    var colNome = $("<td style=\"font-weight: bolder;\"></td>").text(jogador.nickname);
                    $(linha).append(colNome);
                    var colSolo = $("<td ></td>").text(jogador.solo);
                    $(linha).append(colSolo);
                    var colDuo = $("<td ></td>").text(jogador.duo);
                    $(linha).append(colDuo);
                    var colSquad = $("<td ></td>").text(jogador.squad);
                    $(linha).append(colSquad);
                    var kill = $("<td></td>").text(jogador.kill);
                    $(linha).append(kill);
                    var colVit = $("<td style=\"background-color:#EFEFEF\"></td>").text(jogador.vitorias);
                    $(linha).append(colVit);
                    $("#tabela-geral").append(linha);
                    i++;
                }
            });
        });
    });
